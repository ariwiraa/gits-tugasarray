package main

import (
	"fmt"
	"sort"
)

func main() {
	var inputName string
	var subMenu, tambahData int
	var inputRating float64
	var listGames = make(map[int]map[string]float64)

	for {
		fmt.Println("===Sub Menu===")
		fmt.Println("1. Memasukan data\n2. Mendelete data\n3. Menampilkan data")
		fmt.Println("4. Menampilkan data berdasarkan nama\n5. 3 Favorit\n6. Top 3 Rating\n0. Keluar")
		fmt.Print("Pilih menu : ")
		fmt.Scan(&subMenu)

		if subMenu == 1 {
			fmt.Print("Ingin memasukan berapa data? ")
			fmt.Scan(&tambahData)
			for i := 1; i <= tambahData; i++ {
				fmt.Print("Masukan nama game : ")
				fmt.Scan(&inputName)
				fmt.Print("Masukan rating game: ")
				fmt.Scan(&inputRating)
				convertRating := inputRating
				listGames[i] = map[string]float64{
					inputName: convertRating,
				}

				if convertRating > 4.0 {
					fmt.Println("good")
				} else if convertRating <= 4.0 {
					fmt.Println("Average")
				} else if convertRating <= 2.0 {
					fmt.Println("poor")
				}

			}

			for id, games := range listGames {
				for name, rating := range games {

					fmt.Println(id, "|| Nama game :", name, "|| Rating :", rating)
				}
			}

		} else if subMenu == 2 {

			var inputId int
			fmt.Println("Masukan ID yang ingin di delete : ")
			fmt.Scan(&inputId)

			delete(listGames, inputId)

			for id, games := range listGames {
				for name, rating := range games {
					fmt.Println(id, "|| Nama game :", name, "|| Rating :", rating)
				}
			}

		} else if subMenu == 3 {
			for id, games := range listGames {
				for name, rating := range games {
					fmt.Println(id, "|| Nama game :", name, "|| Rating :", rating)
				}
			}
			fmt.Println("Jumlah data :", len(listGames))

		} else if subMenu == 4 {
			for _, games := range listGames {
				for name := range games {
					fmt.Println("Nama game :", name)
				}
			}

		} else if subMenu == 5 {

			for _, games := range listGames {
				for name, rating := range games {
					var topRating []float64
					topRating = append(topRating, rating)

					sort.Float64s(topRating)
					fmt.Println(name, topRating)

				}
			}
		} else if subMenu == 6 {
			for _, games := range listGames {
				for name, rating := range games {
					if rating > 4.0 {
						var topRating []float64

						topRating = append(topRating, rating)
						fmt.Println(name, topRating)
					}
				}
			}

		} else if subMenu == 0 {
			break
		} else {
			fmt.Println("Tidak pilihan tersebut")
		}

	}

}
